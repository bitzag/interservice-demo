package com.demandbridge.interservicedemo.asset;

import com.feign.client.AssetClient;
import com.feign.dto.asset.AssetEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/assets")
@RequiredArgsConstructor
public class AssetController {

    private final AssetClient assetClient;

    @GetMapping(value = "/events/{companyId}", produces = "application/json")
    public Page<AssetEvent> getAssetEvents(@PathVariable Long companyId, Pageable pageable) {
        return assetClient.getAssetEventsForCompany(
                companyId,
                pageable.getPageNumber(),
                pageable.getPageSize(),
                LocalDateTime.now(),
                LocalDateTime.now().minusYears(1),
                ""
        );
    }
}
