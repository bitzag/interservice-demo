package com.demandbridge.interservicedemo.orderhistory;

import com.feign.dto.accounthistory.AccountHistoryOrderDetailResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient("dbe-account-history")
public interface OrderHistoryClient {

    @GetMapping("/api/v1/orders/{orderId}/details")
    AccountHistoryOrderDetailResponse getOrderDetails(@PathVariable Integer orderId);
}
