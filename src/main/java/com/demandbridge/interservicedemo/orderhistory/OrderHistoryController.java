package com.demandbridge.interservicedemo.orderhistory;

import com.feign.dto.accounthistory.AccountHistoryOrderDetailResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order-history/orders")
@RequiredArgsConstructor
public class OrderHistoryController {

    private final OrderHistoryService orderHistoryService;

    @GetMapping(value = "/{orderId}", produces = "application/json")
    public AccountHistoryOrderDetailResponse getOrderDetails(@PathVariable int orderId) {
        return orderHistoryService.getOrderDetails(orderId);
    }
}
