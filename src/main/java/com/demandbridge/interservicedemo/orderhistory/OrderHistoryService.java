package com.demandbridge.interservicedemo.orderhistory;

import com.feign.client.AccountHistoryClient;
import com.feign.dto.accounthistory.AccountHistoryOrderDetailResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderHistoryService {

        private final AccountHistoryClient accountHistoryClient;
//    private final OrderHistoryClient accountHistoryClient;

    public AccountHistoryOrderDetailResponse getOrderDetails(int orderId) {
        return accountHistoryClient.getOrderDetails(orderId);
    }

}
