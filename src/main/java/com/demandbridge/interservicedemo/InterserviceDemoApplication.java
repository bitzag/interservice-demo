package com.demandbridge.interservicedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = { "com.feign", "com.demandbridge"})
@EnableEurekaClient
@SpringBootApplication(scanBasePackages = { "com.demandbridge"})
public class InterserviceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(InterserviceDemoApplication.class, args);
    }

}
