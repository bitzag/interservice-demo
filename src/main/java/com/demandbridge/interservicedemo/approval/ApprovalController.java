package com.demandbridge.interservicedemo.approval;

import com.feign.client.ApprovalClient;
import com.feign.dto.approval.ApprovalCacheDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ApprovalController {

    private final ApprovalClient approvalClient;

    @GetMapping(value = "/orders/{orderId}/approvals", produces = "application/json")
    public List<ApprovalCacheDetails> getApprovalsForOrder(@PathVariable Long orderId) {
        return approvalClient.getApprovalCachesForOrder(orderId);
    }
}
