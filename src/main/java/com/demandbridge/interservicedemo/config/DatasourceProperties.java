package com.demandbridge.interservicedemo.config;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Validated
@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
public class DatasourceProperties {

    @NotNull
    @NotBlank(message = "Datasource username is required")
    private String username;

    @NotNull
    @NotBlank(message = "Datasource password is required")
    private String password;

    @NotNull
    @NotBlank(message = "Datasource url is required")
    private String url;

    private String driverClassName;
}
