package com.demandbridge.interservicedemo.config;

import lombok.RequiredArgsConstructor;
import oracle.jdbc.datasource.impl.OracleDataSource;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;
import java.sql.SQLException;

@Profile("!test")
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(value = DatasourceProperties.class)
public class OracleConfig {

    private final DatasourceProperties datasourceProperties;

    @Bean
    public DataSource dataSource() throws SQLException {
        var datasource = new OracleDataSource();
        datasource.setUser(datasourceProperties.getUsername());
        datasource.setPassword(datasourceProperties.getPassword());
        datasource.setURL(datasourceProperties.getUrl());
        datasource.setDriverType(datasourceProperties.getDriverClassName());
        datasource.setImplicitCachingEnabled(true);
        return datasource;
    }
}
